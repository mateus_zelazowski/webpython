
from flask import Blueprint, render_template, request, redirect, url_for
from mod_produto.ProdutoBD  import Produtos
import pymysql

import base64

bp_produto = Blueprint('produto', __name__, url_prefix='/produto', template_folder='templates')

@bp_produto.route("/", methods=['GET', 'POST'])
def formListaProdutos():
    produto=Produtos()
    res=produto.selectALL()
    return render_template('listaProd.html', result=res, content_type='application/json')


@bp_produto.route("/formProduto", methods=['POST'])
def formProduto():
    produto=Produtos()
    return render_template('formProd.html', produto=produto, content_type='application/json')


@bp_produto.route('/addProduto', methods=['POST'])
def addProduto():
    produto=Produtos()
    produto.id_produto = request.form['id_produto']
    produto.descricao = request.form['descricao']
    produto.valor = request.form['valor']

    produto.imagem =  "data:" + request.files['imagem'].content_type + ";base64," + str(base64.b64encode( request.files['imagem'].read() ) , "utf-8")
  
    produto.insert()
    return redirect('/produto')


@bp_produto.route('/formEditProduto', methods=['POST'])
def formEditProduto():
    produto=Produtos()
    #realiza a busca pelo produto e armazena o resultado no objeto
    produto.selectONE( request.form['id_produto'] )
    return render_template('formProd.html', produto=produto, content_type='application/json')

@bp_produto.route('/editProduto', methods=['POST'])
def editProduto():
    produto=Produtos()
    produto.id_produto = request.form['id_produto']
    produto.descricao = request.form['descricao']
    produto.valor = request.form['valor']
    produto.imagem =  "data:" + request.files['imagem'].content_type + ";base64," + str(base64.b64encode( request.files['imagem'].read() ) , "utf-8")

    if 'salvaEditaProdutoDB' in request.form:
        produto.update()
    elif 'salvaExcluiProdutoDB' in request.form:
        produto.delete()
    
    return redirect('/produto')