from flask import Blueprint, render_template, redirect, url_for, request, session
from functools import wraps
login = Blueprint('login', __name__, url_prefix="/", template_folder='templates')
from mod_cliente.ClienteBD import Clientes
@login.route("/", methods=[ 'GET', 'POST'])
def login1():
    return render_template("formLogin.html")
@login.route("/login", methods=[ 'POST'])
def validaLogin():
    _name = request.form['username']
    _pass = request.form['password']
    ValidaUserBanco = Clientes()
    userBanco = ValidaUserBanco.validaUsuario( _name , _pass )

    userNameDB=0
    passwordDB=0
    grupoDB=0
     
    for row in userBanco:
          id = row[0]
          userNameDB=row[1]
          passwordDB=row[2]
          grupoDB=row[3]
          nomeDB=row[4]
    if _name and _pass and request.method == 'POST' and _name == userNameDB and _pass == passwordDB:
        #limpa a sessão
        session.clear()
        #registra usuario na sessão, armazenando o login do usuário
        session['username'] = _name
        session['grupo'] = grupoDB
        session['nome'] = nomeDB
        #abre a aplicação na tela home
        return redirect(url_for('home.home'))
    else:
        #retorna para a tela de login
        return redirect(url_for('login.login1',falhaLogin=1))



@login.route("/logoff",methods=['get'])
def logoff():
     session.pop('username',None)
     session.clear()
     return redirect(url_for('login.login1'))   
def validaSessao(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'username' not in session:
            #descarta os dados copiados da função original e retorna a tela de login
            return redirect(url_for('login.login1',falhaSessao=1))
        else:
            #retorna os dados copiados da função original
            return f(*args, **kwargs)
    #retorna o resultado do if acima
    return decorated_function       
