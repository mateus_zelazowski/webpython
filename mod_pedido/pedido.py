from flask import Blueprint, render_template, request, redirect, url_for, make_response
from mod_login.login import validaSessao
from mod_cliente.ClienteBD import Clientes
from mod_produto.ProdutoBD import Produtos
from mod_pedido.pedidoBD import Pedido
from mod_pedido.pedidoProd import PedidoProd
from base64 import b64encode
import pdfkit


bp_pedido = Blueprint('pedido', __name__, url_prefix="/pedido", template_folder='templates')

itensped = []


@bp_pedido.route("/", methods=['GET', 'POST'])
@validaSessao
def formListaPedidos():
    ped= Pedido()
    res= ped.selectAll()
    print(res)

    return render_template("listaPedido.html", result=res, content_type='application/json')

    

@bp_pedido.route("/formPedido", methods=['GET','POST'])
@validaSessao
def formPedido():
     
    cliente = Clientes()
    clientes = cliente.selectClientesALL()


    if request.form:
        pedido=Pedido( request.form['observacoes'], request.form['code'])
        
        retorno = pedido.insert()
        if retorno=='Pedido cadastrado com sucesso!':
            return redirect(url_for('pedido.formProdutosPed', id=pedido.id))
        
    return render_template("formpedido.html", clientes=clientes)

@bp_pedido.route("/formProdutosPed/<int:id>", methods=['GET','POST'])
def formProdutosPed(id):

    produto = Produto()
    produtos = produto.selectALL()

    cliente = Clientes()
    clientes = cliente.selectClientesALL()

    itemped = PedidoProd()
    itenspedidos = itemped.PedidoProd(id)

    pedido = Pedido()
    pedido.selectPedido(id)
    print(pedido)


    if 'addProdutoDB' in request.form:
        itenspedidos = PedidoProd(
            id,
            request.form['idprod'],
            request.form['quantidade'],
            request.form['valor'],
            request.form['observacoes']
        )
        itenspedidos.insert()
        return redirect(url_for("pedido.formProdutosPed", id= id))
       


    if 'salvarpedido' in request.form:
        for ip in itensped:
            print(ip.produtos_id)
            print(id)
            print(ip.observacao)
            print(ip.quantidade)
            print(ip.valor)
            

    return render_template("formeditpedido.html", produtos=produtos, clientes=clientes, pedido=pedido, itensped=itenspedidos)


@bp_pedido.route("/UpPed/<int:id>", methods=['GET','POST'])
def UpPed(id):
    
    if request.form:
        pedido=Pedido( request.form['observacoes'], request.form['code'])
        
        pedido.id=id
        retorno = pedido.update()
        
            
    return redirect(url_for('pedido.formProdutosPed', id=id))

@bp_pedido.route("/DeleteProdutoPed", methods=['GET','POST'])
def DeleteProdutoPed():

    if request.form:
        produtosped = PedidoProd()
        produtosped.pedidos_id= request.form['idPedido']
        produtosped.produtos_id= request.form['idProdutoP']
        produtosped.delete()

    return redirect(url_for('pedido.formProdutosPed', id= request.form['idPedido']))

@bp_pedido.route("/DeletePed/<int:id>", methods=['GET','POST'])
def DeletePed(id):

    
    pedidos = Pedido()
    pedidos.delete(id)

    return redirect(url_for('pedido.formListaPedidos'))


@bp_pedido.route('/report/<int:id>')
def report(id):

    order = Pedido()
    ret = order.selectPedido(id)
    if not order.id:
        flash(ret, 'info')
        return redirect(url_for('pedido.formListaPedidos'))

    order_p = PedidoProd()
    products = order_p.selectALL(order.id)
    images = []
    for product in products:
        images.append(product[7])

    ren = render_template('gerarPDF.html', products=products, images=images, order=order)
    pdf = pdfkit.from_string(ren, False)
    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'attachement; filename=relatorio-pedido.pdf'
    return response