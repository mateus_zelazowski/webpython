import os
from flask import Flask, session
from datetime import timedelta

from mod_login.login import login
from mod_home.home import bp_home
from mod_cliente.cliente import bp_cliente
from mod_produto.produto import bp_produto
from mod_pedido.pedido import bp_pedido




app = Flask(__name__)

@app.before_request
def before_request():
    session.permanent = True
    #o padrão é 31 dias...
    app.permanent_session_lifetime = timedelta(minutes=60)
app.register_blueprint(login)
app.register_blueprint(bp_home)
app.register_blueprint(bp_cliente)
app.register_blueprint(bp_produto)
app.register_blueprint(bp_pedido)

app.secret_key = os.urandom(12).hex()
if __name__ == '__main__':
    app.run()

